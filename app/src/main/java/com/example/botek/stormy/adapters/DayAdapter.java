package com.example.botek.stormy.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.botek.stormy.R;
import com.example.botek.stormy.Weather.Day;

public class DayAdapter extends BaseAdapter {
     // Ky adapter duhet me i dit kontekstin dhe te dhenat qe do t'i pasqyroje
    private Context mContext;
    private Day[] mDays;

    public DayAdapter(Context context, Day[] days){

        mContext = context;
        mDays = days;
    }

    @Override
    public int getCount() {
        return mDays.length;

    }

    @Override
    public Object getItem(int position) {
        return mDays[position]; // Parametri position mer te dhenat dhe i vendos ne view listen e cakutar

    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView==null){
            //brand new
            convertView = LayoutInflater.from(mContext).inflate(R.layout.daily_list_item, null);
            holder =new ViewHolder();
            holder.iconImageView = (ImageView)convertView.findViewById(R.id.iconImageView);
            holder.temperatureLabel = (TextView)convertView.findViewById(R.id.temperatureLabel);
            holder.dayLabel = (TextView)convertView.findViewById(R.id.dayNameLabel);

            convertView.setTag(holder);

        }
        else{

            // e anashkalojmme thirrjen findViewById() nga res ne cdo kohe
            // perdorim viewHolder

            holder = (ViewHolder) convertView.getTag();
        }

        Day day = mDays[position];
        holder.iconImageView.setImageResource(day.getIconId());
        holder.temperatureLabel.setText(day.getTemperatureMax()+"");

        if(position==0){
            holder.dayLabel.setText("Today");
        }
        else {
            holder.dayLabel.setText(day.getDayOfTheWeek());
        }
        return convertView;
    }


    public static class ViewHolder{
        ImageView iconImageView; //public by default
        TextView temperatureLabel;
        TextView dayLabel;

    }

}

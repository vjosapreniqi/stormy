package com.example.botek.stormy.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;

public class AlertDialogFragment1 extends DialogFragment {


    @Override

    public Dialog onCreateDialog(Bundle savedInstanceState){
        Context context= getActivity();
        AlertDialog.Builder builder=new AlertDialog.Builder(context)
                .setTitle("No connectivity!")
                .setMessage("No connectivity, Please turn on the internet.")
                .setPositiveButton("OK",null);
        AlertDialog dialog = builder.create();
        return dialog;

    }


}
